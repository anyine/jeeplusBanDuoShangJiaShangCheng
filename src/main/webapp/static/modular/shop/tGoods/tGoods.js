/**
 * 商品管理初始化
 */
var TGoods = {
    id: "TGoodsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
TGoods.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},

        {title: 'title', field: 'title', align: 'center', valign: 'middle'},
        {title: '标签', field: 'tag', align: 'center', valign: 'middle'},
        {title: '价格', field: 'prices', align: 'center', valign: 'middle'},
        {title: '创建时间', field: 'createDate', align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
TGoods.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        TGoods.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商品
 */
TGoods.openAddTGoods = function () {
    var index = layer.open({
        type: 2,
        title: '添加商品',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/tGoods/tGoods_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商品详情
 */
TGoods.openTGoodsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商品详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/tGoods/tGoods_update/' + TGoods.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除商品
 */
TGoods.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/tGoods/delete", function (data) {
            Feng.success("删除成功!");
            TGoods.table.refresh();
        }, function (data) {
            Feng.error("删除失败!");
        });
        ajax.set("id", this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询商品列表
 */
TGoods.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    TGoods.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = TGoods.initColumn();
    var table = new BSTable(TGoods.id, "/tGoods/list", defaultColunms);
    table.setPaginationType("client");
    TGoods.table = table.init();
});
