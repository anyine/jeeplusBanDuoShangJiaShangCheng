/**
 * 部门管理初始化
 */
var Link = {
    id: "linkTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Link.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'id', align: 'center', valign: 'middle'},
        {title: '链接简称', field: 'linkName', align: 'center', valign: 'middle'},
        {title: '链接', field: 'linkUrl', align: 'center', valign: 'middle'},
        {title: '排序', field: 'orderNo', align: 'center', valign: 'middle'}];
};

/**
 * 检查是否选中
 */
Link.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Link.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加部门
 */
Link.openAddLink = function () {
    var index = layer.open({
        type: 2,
        title: '添加链接',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/link/link_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看部门详情
 */
Link.openLinkDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '链接详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/link/link_update/' + Link.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除部门
 */
Link.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/link/delete", function (data) {
            Feng.success("删除成功!");
            Link.table.refresh();
        }, function (data) {
            Feng.error("删除失败!");
        });
        ajax.set("linkId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询链接列表
 */
Link.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Link.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Link.initColumn();
    var table = new BSTable(Link.id, "/link/list", defaultColunms);
    table.setPaginationType("client");
    Link.table = table.init();
});
