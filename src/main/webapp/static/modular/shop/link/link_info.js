/**
 * 初始化部门详情对话框
 */
var LinkInfoDlg = {
    linkInfoData : {},
    zTreeInstance : null
};

/**
 * 清除数据
 */
LinkInfoDlg.clearData = function() {
    this.linkInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LinkInfoDlg.set = function(key, val) {
    this.linkInfoData[key] = (typeof value == "undefined") ? $("#" + key).val() : value;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LinkInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LinkInfoDlg.close = function() {
    parent.layer.close(window.parent.Link.layerIndex);
}


/**
 * 收集数据
 */
LinkInfoDlg.collectData = function() {
    this.set('id').set('linkName').set('linkUrl').set('orderNo');
}

/**
 * 提交添加部门
 */
LinkInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/link/add", function(data){
        Feng.success("添加成功!");
        window.parent.Link.table.refresh();
        LinkInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.linkInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LinkInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/link/update", function(data){
        Feng.success("修改成功!");
        window.parent.Link.table.refresh();
        LinkInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.linkInfoData);
    ajax.start();
}

function onBodyDown(event) {
    if (!(event.target.id == "menuBtn" || event.target.id == "parentLinkMenu" || $(
            event.target).parents("#parentLinkMenu").length > 0)) {
        LinkInfoDlg.hideLinkSelectTree();
    }
}
