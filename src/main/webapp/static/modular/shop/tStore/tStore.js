/**
 * 部门管理初始化
 */
var TArticle = {
    id: "tStoreTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
TArticle.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'id', align: 'center', valign: 'middle'},
        {title: '简称', field: 'storeName', align: 'center', valign: 'middle'},
        {title: '掌柜', field: 'storeOwer', align: 'center', valign: 'middle'},
        {title: '地址', field: 'storeAddress', align: 'center', valign: 'middle'},
        {title: '时间', field: 'addTime', align: 'center', valign: 'middle'}];
};

/**
 * 检查是否选中
 */
TArticle.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        TArticle.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加部门
 */
TArticle.openAddTArticle = function () {
    var index = layer.open({
        type: 2,
        title: '添加链接',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/tStore/tStore_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看部门详情
 */
TArticle.openTArticleDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '链接详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/tStore/tStore_update/' + TArticle.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除部门
 */
TArticle.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/tStore/delete", function (data) {
            Feng.success("删除成功!");
            TArticle.table.refresh();
        }, function (data) {
            Feng.error("删除失败!");
        });
        ajax.set("tStoreId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询链接列表
 */
TArticle.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    TArticle.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = TArticle.initColumn();
    var table = new BSTable(TArticle.id, "/tStore/list", defaultColunms);
    table.setPaginationType("client");
    TArticle.table = table.init();
});
