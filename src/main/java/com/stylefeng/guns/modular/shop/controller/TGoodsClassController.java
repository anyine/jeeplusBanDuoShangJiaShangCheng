package com.stylefeng.guns.modular.shop.controller;

import com.stylefeng.guns.common.annotion.Permission;
import com.stylefeng.guns.common.annotion.log.BussinessLog;
import com.stylefeng.guns.common.constant.Const;
import com.stylefeng.guns.common.constant.Dict;
import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.controller.BaseController;
import com.stylefeng.guns.common.exception.BizExceptionEnum;
import com.stylefeng.guns.common.exception.BussinessException;
import com.stylefeng.guns.common.node.ZTreeNode;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.shop.service.ITGoodsClassService;
import com.stylefeng.guns.modular.shop.warpper.GoodsClassWarpper;
import com.stylefeng.guns.modular.system.warpper.DeptWarpper;
import com.stylefeng.guns.persistence.shop.model.TGoodsClass;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品分类表 前端控制器
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Controller
@RequestMapping("/tGoodsClass")
public class TGoodsClassController extends BaseController {
    private String PREFIX = "/shop/tGoodsClass/";

    @Resource
    ITGoodsClassService itGoodsClassService;


    /**
     * 跳转到部门管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "tGoodsClass.html";
    }

    /**
     * 跳转到添加部门
     */
    @RequestMapping("/tGoodsClass_add")
    public String tGoodsClassAdd() {
        return PREFIX + "tGoodsClass_add.html";
    }

    /**
     * 跳转到修改部门
     */
    @RequestMapping("/tGoodsClass_update/{tGoodsClassId}")
    public String tGoodsClassUpdate(@PathVariable Integer tGoodsClassId, Model model) {
        TGoodsClass tGoodsClass = itGoodsClassService.selectById(tGoodsClassId);
        model.addAttribute("tGoodsClass",tGoodsClass);
        TGoodsClass ptGoodsClass = itGoodsClassService.selectById(tGoodsClass.getPid());
        model.addAttribute("pName", ptGoodsClass.getName());
        LogObjectHolder.me().set(tGoodsClass);
        return PREFIX + "tGoodsClass_edit.html";
    }
    /**
     * 获取部门的tree列表
     */
    @RequestMapping(value = "/tree")
    @ResponseBody
    public List<ZTreeNode> tree() {
        List<ZTreeNode> tree = this.itGoodsClassService.tree();
        tree.add(ZTreeNode.createParent());
        return tree;
    }

    /**
     * 新增部门
     */
    @BussinessLog(value = "添加部门", key = "simplename", dict = Dict.DeptDict)
    @RequestMapping(value = "/add")
    @ResponseBody
    @Permission(Const.ADMIN_NAME)
    public Object add(TGoodsClass tGoodsClass) {
        return this.itGoodsClassService.insert(tGoodsClass);
    }

    /**
     * 获取所有部门列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        List<Map<String, Object>> list = this.itGoodsClassService.selectMaps(null);
        return super.warpObject(new GoodsClassWarpper(list));
    }

    /**
     * 部门详情
     */
    @RequestMapping(value = "/detail/{tGoodsClassId}")
    @ResponseBody
    public Object detail(@PathVariable("tGoodsClassId") Integer tGoodsClassId) {
        return itGoodsClassService.selectById(tGoodsClassId);
    }

    /**
     * 修改部门
     */
    @BussinessLog(value = "修改部门", key = "simplename", dict = Dict.DeptDict)
    @RequestMapping(value = "/update")
    @ResponseBody
    @Permission(Const.ADMIN_NAME)
    public Object update(TGoodsClass tGoodsClass) {
        if (ToolUtil.isEmpty(tGoodsClass) || tGoodsClass.getId() == null) {
            throw new BussinessException(BizExceptionEnum.REQUEST_NULL);
        }
        itGoodsClassService.updateById(tGoodsClass);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除部门
     */
    @BussinessLog(value = "删除部门", key = "tGoodsClassId", dict = Dict.DeleteDict)
    @RequestMapping(value = "/delete")
    @ResponseBody
    @Permission(Const.ADMIN_NAME)
    public Object delete(@RequestParam Long tGoodsClassId) {

        itGoodsClassService.deleteById(tGoodsClassId);

        return SUCCESS_TIP;
    }
}
