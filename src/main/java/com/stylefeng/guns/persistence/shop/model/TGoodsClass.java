package com.stylefeng.guns.persistence.shop.model;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 商品分类表
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@TableName("t_goods_class")
public class TGoodsClass extends Model<TGoodsClass> {

    private static final long serialVersionUID = 1L;

    /**
     * 索引ID
     */
	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 分类名称
     */
	private String name;
    /**
     * 分类图片
     */
	private String pic;
    /**
     * 前台显示，0为否，1为是，默认为1
     */
	@TableField("del_flag")
	private String delFlag;
    /**
     * 名称
     */
	private String title;

	private Long pid;
	@TableField("parent_ids")
	private String parentIds;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public String getParentIds() {
		return parentIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
